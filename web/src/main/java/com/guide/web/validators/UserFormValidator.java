package com.guide.web.validators;

import com.guide.web.dto.UserForm;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Service
public class UserFormValidator implements Validator {
    @Override
    public boolean supports(final Class<?> aClass) {
        return UserForm.class.equals(aClass);
    }

    @Override
    public void validate(final Object o, final Errors errors) {
        UserForm userForm = (UserForm) o;
        if (!userForm.getPassword().equals(userForm.getPasswordCopy())){
            errors.rejectValue("passwordCopy", "mismatch", "\nВведенные пароли не совпадают");
        }
    }
}
