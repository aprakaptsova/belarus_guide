package com.guide.web.services;

import com.guide.web.converters.MarkerToMarkerInfoConverter;
import com.guide.web.dto.MarkerInfo;
import com.guide.web.models.Marker;
import com.guide.web.repo.MarkerRepository;
import org.springframework.stereotype.Service;

@Service
public class MarkerService {

    private MarkerRepository markerRepository;
    private MarkerToMarkerInfoConverter markerToMarkerInfoConverter;

    public MarkerService(final MarkerRepository markerRepository,
                         final MarkerToMarkerInfoConverter markerToMarkerInfoConverter) {
        this.markerRepository = markerRepository;
        this.markerToMarkerInfoConverter = markerToMarkerInfoConverter;
    }

    public void add(Marker marker){
        markerRepository.save(marker);
    }

    protected Marker get(long id){
        return markerRepository.findById(id).get();
    }

    protected MarkerInfo getMarkerInfoByMarker(Marker marker){
        return markerToMarkerInfoConverter.convert(marker);
    }
}
