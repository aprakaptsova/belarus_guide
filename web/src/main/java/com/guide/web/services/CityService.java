package com.guide.web.services;

import com.guide.web.converters.CityFormToCityConverter;
import com.guide.web.converters.CityToCityFormConverter;
import com.guide.web.converters.CityToCityInfoConverter;
import com.guide.web.dto.CityForm;
import com.guide.web.dto.CityInfo;
import com.guide.web.models.City;
import com.guide.web.models.CityImage;
import com.guide.web.models.Region;
import com.guide.web.repo.CityImageRepository;
import com.guide.web.repo.CityRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class CityService {

    private CityToCityInfoConverter cityToCityInfoConverter;
    private CityToCityFormConverter cityToCityFormConverter;
    private RegionService regionService;
    private CityFormToCityConverter cityFormToCityConverter;
    private CityRepository cityRepository;
    private CityImageRepository cityImageRepository;

    public CityService(final CityToCityInfoConverter cityToCityInfoConverter,
                       final RegionService regionService,
                       final CityFormToCityConverter cityFormToCityConverter,
                       final CityRepository cityRepository,
                       final CityToCityFormConverter cityToCityFormConverter,
                       final CityImageRepository cityImageRepository) {
        this.cityToCityInfoConverter = cityToCityInfoConverter;
        this.regionService = regionService;
        this.cityFormToCityConverter = cityFormToCityConverter;
        this.cityRepository = cityRepository;
        this.cityToCityFormConverter = cityToCityFormConverter;
        this.cityImageRepository = cityImageRepository;
    }

    public List<CityInfo> getCitiesByRegion(long regionId) {
        Region region = regionService.get(regionId);
        Set<City> cities = region.getCities();
        List<CityInfo> cityInfos = new ArrayList<>();
        for (City city : cities) {
            CityInfo cityInfo = cityToCityInfoConverter.convert(city);
            cityInfo.setRegion(region.getName());
            cityInfos.add(cityInfo);
        }
        return cityInfos;
    }

    public void add(CityForm cityForm) {
        City city = cityFormToCityConverter.convert(cityForm);
        city.setRegion(regionService.get(cityForm.getRegion()));
        city = cityRepository.save(city);
        if (cityForm.getPhoto() != null) {
            city.setImages(geCityImages(cityForm, city));
            cityRepository.save(city);
        }
    }

    public City get(long id) {
        return cityRepository.findById(id).get();
    }


    public CityForm getEditForm(long cityId) {
        return cityToCityFormConverter.convert(get(cityId));
    }

    public void update(long id, CityForm cityForm) {
        cityRepository.deleteRegionImagesById(id);
        City city = cityFormToCityConverter.convert(cityForm);
        city.setRegion(regionService.get(cityForm.getRegion()));
        city.setId(id);
        if (cityForm.getPhoto() != null) {
            city.setImages(geCityImages(cityForm, city));
        }
        cityRepository.save(city);
    }

    private Set<CityImage> geCityImages(final CityForm cityForm, City city) {
        Set<CityImage> images = new HashSet<>();
        for (String url : cityForm.getPhoto()) {
            CityImage cityImage = new CityImage();
            cityImage.setUrl(url);
            cityImage.setCity(city);
            cityImageRepository.save(cityImage);
            images.add(cityImage);
        }
        return images;
    }

    public CityInfo getCityInfo(long cityId) {
        return cityToCityInfoConverter.convert(get(cityId));
    }
}


