package com.guide.web.services;

import com.guide.web.converters.RouteToRouteInfoConverter;
import com.guide.web.dto.PlaceInfo;
import com.guide.web.dto.RouteInfo;
import com.guide.web.models.Place;
import com.guide.web.models.Route;
import com.guide.web.repo.PlaceRepository;
import com.guide.web.repo.RouteRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class RouteService {
    private RouteToRouteInfoConverter routeToRouteInfoConverter;
    private RouteRepository routeRepository;
    private PlaceService placeService;
    private UserService userService;
    private PlaceRepository placeRepository;

    public RouteService(final RouteToRouteInfoConverter routeToRouteInfoConverter,
                        final RouteRepository routeRepository,
                        final PlaceService placeService,
                        final UserService userService,
                        final PlaceRepository placeRepository) {
        this.routeToRouteInfoConverter = routeToRouteInfoConverter;
        this.routeRepository = routeRepository;
        this.placeService = placeService;
        this.userService = userService;
        this.placeRepository = placeRepository;
    }

    public Route get(long id) {
        return routeRepository.findById(id).get();
    }

    public RouteInfo getRouteInfo(long id) {
        Route route = get(id);
        RouteInfo routeInfo = routeToRouteInfoConverter.convert(route);
        List<Place> places = route.getPlaces();
        List<PlaceInfo> placesInfo = new ArrayList<>();
        for (Place place : places) {
            placesInfo.add(placeService.getPlaceInfo(place.getId()));
        }
        routeInfo.setPlacesInfo(placesInfo);
        return routeInfo;
    }

    public List<RouteInfo> getRoutesInfoByUserId (long userId) {
        return getRoutesInfoByRoutes(routeRepository.getRoutesByUserId(userId));
    }

    public List<RouteInfo> getRoutesInfoByRoutes(List<Route> routes) {
        List<RouteInfo> routesInfo = new ArrayList<>();
        routes.forEach(route -> routesInfo.add(routeToRouteInfoConverter.convert(route)));
        return routesInfo;
    }

    public void saveRoute(List<Long> chosenBalloons, UserDetails user) {
        Route route = new Route();
        route = routeRepository.save(route);
        route.setUser(userService.getUserByEmail(user.getUsername()));
        List<Place> places = new ArrayList<>();
        for (long balloon : chosenBalloons) {
            places.add(placeRepository.findById(balloon).get());
        }
        route.setPlaces(places);
        routeRepository.save(route);
    }

    public void updateRoute(RouteInfo routeInfo) {
        Route route = new Route();
        route.setId(routeInfo.getId());
        route.setUser(routeRepository.findById(routeInfo.getId()).get().getUser());
        List<PlaceInfo> placesInfo = routeInfo.getPlacesInfo();
        List<Place> places = new ArrayList<>();
        placesInfo.forEach(placeInfo -> places.add(placeRepository.findById(placeInfo.getId()).get()));
        route.setPlaces(places);
        routeRepository.save(route);
    }

    public void deleteRoute(long routeId) {
        routeRepository.deleteById(routeId);
    }
}
