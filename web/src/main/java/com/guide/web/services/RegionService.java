package com.guide.web.services;

import com.guide.web.converters.RegionFormToRegionConverter;
import com.guide.web.converters.RegionToRegionFormConverter;
import com.guide.web.converters.RegionToRegionShortInfoConverter;
import com.guide.web.dto.RegionForm;
import com.guide.web.dto.RegionShortInfo;
import com.guide.web.models.Region;
import com.guide.web.models.RegionImage;
import com.guide.web.repo.RegionImageRepository;
import com.guide.web.repo.RegionRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class RegionService {

    private RegionRepository regionRepository;
    private RegionFormToRegionConverter regionFormToRegionConverter;
    private RegionToRegionFormConverter regionToRegionFormConverter;
    private RegionToRegionShortInfoConverter regionToRegionShortInfoConverter;
    private RegionImageRepository imageRepository;

    public RegionService(final RegionRepository regionRepository,
                         final RegionFormToRegionConverter regionFormToRegionConverter,
                         final RegionToRegionFormConverter regionToRegionFormConverter,
                         final RegionToRegionShortInfoConverter regionToRegionShortInfoConverter,
                         final RegionImageRepository imageRepository) {
        this.regionRepository = regionRepository;
        this.regionFormToRegionConverter = regionFormToRegionConverter;
        this.regionToRegionFormConverter = regionToRegionFormConverter;
        this.regionToRegionShortInfoConverter = regionToRegionShortInfoConverter;
        this.imageRepository = imageRepository;
    }

    public void add(RegionForm regionForm) {
        Region region = regionFormToRegionConverter.convert(regionForm);
        region = regionRepository.save(region);
        if (regionForm.getPhoto() != null) {
            region.setImages(getRegionImages(regionForm, region));
            regionRepository.save(region);
        }
    }

    private Set<RegionImage> getRegionImages(RegionForm regionForm, Region region) {
        Set<RegionImage> images = new HashSet<>();
        for (String url : regionForm.getPhoto()) {
            RegionImage regionImage = new RegionImage();
            regionImage.setUrl(url);
            regionImage.setRegion(region);
            imageRepository.save(regionImage);
            images.add(regionImage);
        }
        return images;
    }


    public List<RegionForm> getAll() {
        List<RegionForm> regionForms = new ArrayList<>();
        for (Region region : regionRepository.findAll()) {
            regionForms.add(regionToRegionFormConverter.convert(region));
        }
        return regionForms;
    }

    public List<RegionShortInfo> getAllShortInfo() {
        List<RegionShortInfo> regionShortInfos = new ArrayList<>();
        for (Region region : regionRepository.findAll()) {
            regionShortInfos.add(regionToRegionShortInfoConverter.convert(region));
        }
        return regionShortInfos;
    }

    public Region get(long id) {
        return regionRepository.findById(id).get();
    }


    public RegionForm getEditForm(long id) {
        return regionToRegionFormConverter.convert(get(id));
    }

    public void update(long id, RegionForm regionForm) {
        regionRepository.deleteRegionImagesById(id);
        Region region = regionFormToRegionConverter.convert(regionForm);
        region.setId(id);
        if (regionForm.getPhoto() != null) {
            region.setImages(getRegionImages(regionForm, region));
        }
        regionRepository.save(region);
    }

    public RegionShortInfo getRegionShortInfo(long regionId) {
        return regionToRegionShortInfoConverter.convert(get(regionId));
    }
}
