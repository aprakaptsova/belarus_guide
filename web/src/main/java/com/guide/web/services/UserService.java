package com.guide.web.services;

import com.guide.web.converters.RouteToRouteInfoConverter;
import com.guide.web.converters.UserFormToUserConverter;
import com.guide.web.converters.UserToUserInfoConverter;
import com.guide.web.dto.RouteInfo;
import com.guide.web.dto.UserForm;
import com.guide.web.dto.UserInfo;
import com.guide.web.models.Route;
import com.guide.web.models.User;
import com.guide.web.repo.UserRepository;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class UserService {

    private UserRepository userRepository;
    private PasswordEncoder passwordEncoder;
    private UserFormToUserConverter userFormToUserConverter;
    private UserToUserInfoConverter userToUserInfoConverter;
    private RouteService routeService;

    @Lazy
    public UserService(final UserRepository userRepository,
                       final PasswordEncoder passwordEncoder,
                       final UserFormToUserConverter userFormToUserConverter,
                       final UserToUserInfoConverter userToUserInfoConverter,
                       final RouteService routeService) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.userFormToUserConverter = userFormToUserConverter;
        this.userToUserInfoConverter = userToUserInfoConverter;
        this.routeService = routeService;
    }

    public void add(UserForm userForm) {
        userForm.setPassword(passwordEncoder.encode(userForm.getPassword()));
        User user = userFormToUserConverter.convert(userForm);
        userRepository.save(user);
    }

    protected User get(long id) {
        return userRepository.findById(id).get();
    }

    public User getUserByEmail(String email) {
        return userRepository.getUserByEmail(email);
    }

    public UserInfo getUserInfo(long id) {
        User user = get(id);
        UserInfo userInfo = userToUserInfoConverter.convert(user);
        List<Route> routes = user.getRoutes();
        List<RouteInfo> routesInfo = new ArrayList<>();
        for (Route route : routes) {
            routesInfo.add(routeService.getRouteInfo(route.getId()));
        }
        userInfo.setRoutes(routesInfo);
        return userInfo;
    }
}
