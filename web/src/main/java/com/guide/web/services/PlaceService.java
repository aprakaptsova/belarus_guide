package com.guide.web.services;

import com.guide.web.converters.PlaceFormToPlaceConverter;
import com.guide.web.converters.PlaceToPlaceFormConverter;
import com.guide.web.converters.PlaceToPlaceInfoConverter;
import com.guide.web.converters.PlaceToPlaceShortInfoConverter;
import com.guide.web.dto.PlaceForm;
import com.guide.web.dto.PlaceInfo;
import com.guide.web.dto.PlaceShortInfo;
import com.guide.web.models.City;
import com.guide.web.models.Marker;
import com.guide.web.models.Place;
import com.guide.web.models.PlaceImage;
import com.guide.web.repo.PlaceImageRepository;
import com.guide.web.repo.PlaceRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class PlaceService {

    private PlaceFormToPlaceConverter placeFormToPlaceConverter;
    private PlaceToPlaceShortInfoConverter placeToPlaceShortInfoConverter;
    private PlaceToPlaceInfoConverter placeToPlaceInfoConverter;
    private PlaceToPlaceFormConverter placeToPlaceFormConverter;
    private CityService cityService;
    private PlaceRepository placeRepository;
    private MarkerService markerService;
    private PlaceImageRepository placeImageRepository;

    public PlaceService(final PlaceFormToPlaceConverter placeFormToPlaceConverter,
                        final CityService cityService,
                        final PlaceRepository placeRepository,
                        final MarkerService markerService,
                        final PlaceToPlaceShortInfoConverter placeToPlaceShortInfoConverter,
                        final PlaceToPlaceInfoConverter placeToPlaceInfoConverter,
                        final PlaceToPlaceFormConverter placeToPlaceFormConverter,
                        final PlaceImageRepository placeImageRepository) {
        this.placeFormToPlaceConverter = placeFormToPlaceConverter;
        this.cityService = cityService;
        this.placeRepository = placeRepository;
        this.markerService = markerService;
        this.placeToPlaceShortInfoConverter = placeToPlaceShortInfoConverter;
        this.placeToPlaceInfoConverter = placeToPlaceInfoConverter;
        this.placeToPlaceFormConverter = placeToPlaceFormConverter;
        this.placeImageRepository = placeImageRepository;
    }

    public void add(PlaceForm placeForm) {
        Place place = placeFormToPlaceConverter.convert(placeForm);
        place.setMarker(getMarkerByCoords(placeForm));
        place.setCity(cityService.get(placeForm.getCity()));
        placeRepository.save(place);

        if (placeForm.getPhoto() != null) {
            place.setImages(getPlaceImages(placeForm, place));
            placeRepository.save(place);
        }
    }

    //TODO: Возожно стоит вынести в сервис мркера
    private Marker getMarkerByCoords(final PlaceForm placeForm) {
        Marker marker = new Marker();
        marker.setX(placeForm.getX());
        marker.setY(placeForm.getY());
        markerService.add(marker);
        return marker;
    }

    protected Place get(long id) {
        return placeRepository.findById(id).get();
    }

    public List<PlaceShortInfo> getPlacesByCity(long cityId) {
        City city = cityService.get(cityId);
        Set<Place> places = city.getPlaces();
        List<PlaceShortInfo> placeShortInfos = new ArrayList<>();
        for (Place place : places) {
            PlaceShortInfo placeShortInfo = placeToPlaceShortInfoConverter.convert(place);
            placeShortInfos.add(placeShortInfo);
        }
        return placeShortInfos;
    }

    public PlaceInfo getPlaceInfo(long id) {
        Place place = get(id);
        PlaceInfo placeInfo = placeToPlaceInfoConverter.convert(place);
        placeInfo.setMarkerInfo(markerService.getMarkerInfoByMarker(place.getMarker()));
        return placeInfo;
    }

    public PlaceForm getEditForm(long placeId) {
        return placeToPlaceFormConverter.convert(get(placeId));
    }

    public void update(long id, PlaceForm placeForm) {
        placeRepository.deletePlaceImagesById(id);
        Place place = placeFormToPlaceConverter.convert(placeForm);
        place.setCity(cityService.get(placeForm.getCity()));
        place.setMarker(getMarkerByCoords(placeForm));
        place.setId(id);
        if (placeForm.getPhoto() != null) {
            place.setImages(getPlaceImages(placeForm, place));
        }
        placeRepository.save(place);
    }

    private Set<PlaceImage> getPlaceImages(PlaceForm placeForm, Place place) {
        Set<PlaceImage> images = new HashSet<>();
        for (String url : placeForm.getPhoto()) {
            PlaceImage placeImage = new PlaceImage();
            placeImage.setUrl(url);
            placeImage.setPlace(place);
            placeImageRepository.save(placeImage);
            images.add(placeImage);
        }
        return images;
    }

    public List<PlaceShortInfo> getPlacesByWord(String word) {
        List<Place> places = placeRepository.getPlacesByWord(word.toLowerCase());
        List<PlaceShortInfo> placeShortInfos = new ArrayList<>();
        for (Place place : places) {
            placeShortInfos.add(placeToPlaceShortInfoConverter.convert(place));
        }
        return placeShortInfos;
    }
}
