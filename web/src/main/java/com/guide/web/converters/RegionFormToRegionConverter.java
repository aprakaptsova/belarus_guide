package com.guide.web.converters;

import com.guide.web.dto.RegionForm;
import com.guide.web.models.Region;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;

@Service
public class RegionFormToRegionConverter implements Converter<RegionForm, Region> {

    @Override
    public Region convert(final RegionForm regionInfo) {
        Region region = new Region();
        region.setName(regionInfo.getName());
        region.setDescription(regionInfo.getDescription());
        region.setId(regionInfo.getId());
        return region;
    }
}

