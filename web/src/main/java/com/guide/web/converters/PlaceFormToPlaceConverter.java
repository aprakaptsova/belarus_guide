package com.guide.web.converters;

import com.guide.web.dto.PlaceForm;
import com.guide.web.models.Place;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;

@Service
public class PlaceFormToPlaceConverter implements Converter<PlaceForm, Place> {

    @Override
    public Place convert(final PlaceForm placeForm) {
        Place place = new Place();
        place.setName(placeForm.getName());
        place.setAddress(placeForm.getAddress());
        place.setDescription(placeForm.getDescription());
        place.setPhone(placeForm.getPhone());
        place.setSite(placeForm.getSite());
        place.setWorkTime(placeForm.getWorkTime());
        return place;
    }
}
