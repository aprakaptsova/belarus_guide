package com.guide.web.converters;

import com.guide.web.dto.MarkerInfo;
import com.guide.web.models.Marker;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;

@Service
public class MarkerToMarkerInfoConverter implements Converter<Marker, MarkerInfo> {

    @Override
    public MarkerInfo convert(final Marker marker) {
        MarkerInfo markerInfo = new MarkerInfo();
        markerInfo.setId(marker.getId());
        markerInfo.setX(marker.getX());
        markerInfo.setY(marker.getY());
        return markerInfo;
    }
}
