package com.guide.web.converters;

import com.guide.web.dto.UserForm;
import com.guide.web.models.User;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;

@Service
public class UserFormToUserConverter implements Converter<UserForm, User> {
    @Override
    public User convert(final UserForm userForm) {
        User user = new User();
        user.setEmail(userForm.getEmail());
        user.setName(userForm.getName());
        user.setPassword(userForm.getPassword());
        user.setRole('U');
        return user;
    }
}
