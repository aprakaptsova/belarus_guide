package com.guide.web.converters;

import com.guide.web.dto.RegionShortInfo;
import com.guide.web.models.Region;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;

@Service
public class RegionToRegionShortInfoConverter implements Converter<Region, RegionShortInfo> {

    @Override
    public RegionShortInfo convert(final Region region) {
        RegionShortInfo shortInfo = new RegionShortInfo();
        shortInfo.setId(region.getId());
        shortInfo.setName(region.getName());
        return shortInfo;
    }
}
