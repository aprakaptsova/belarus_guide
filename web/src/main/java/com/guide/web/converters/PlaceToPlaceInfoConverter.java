package com.guide.web.converters;

import com.guide.web.dto.PlaceInfo;
import com.guide.web.models.Place;
import com.guide.web.models.PlaceImage;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class PlaceToPlaceInfoConverter implements Converter<Place, PlaceInfo> {

    @Override
    public PlaceInfo convert(final Place place) {
        PlaceInfo placeInfo = new PlaceInfo();
        placeInfo.setId(place.getId());
        placeInfo.setAddress(place.getAddress());
        placeInfo.setDescription(Arrays.asList(place.getDescription().split("\n")));
        placeInfo.setName(place.getName());
        placeInfo.setPhone(place.getPhone());
        placeInfo.setSite(place.getSite());
        placeInfo.setWorkTime(place.getWorkTime());
        placeInfo.setCity(place.getCity().getName());
        placeInfo.setRegion(place.getCity().getRegion().getName());
        List<String> urls = new ArrayList<>();
        for (PlaceImage image : place.getImages()) {
            urls.add(image.getUrl());
        }
        placeInfo.setPhoto(urls);
        return placeInfo;
    }
}
