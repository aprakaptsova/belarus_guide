package com.guide.web.converters;

import com.guide.web.dto.CityInfo;
import com.guide.web.models.City;
import com.guide.web.models.CityImage;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CityToCityInfoConverter implements Converter<City, CityInfo> {

    @Override
    public CityInfo convert(final City city) {
        CityInfo cityInfo = new CityInfo();
        cityInfo.setId(city.getId());
        cityInfo.setName(city.getName());
        cityInfo.setDescription(city.getDescription());
        List<String> urls = new ArrayList<>();
        for (CityImage image : city.getImages()) {
            urls.add(image.getUrl());
        }
        cityInfo.setPhoto(urls);
        return cityInfo;
    }
}
