package com.guide.web.converters;

import com.guide.web.dto.PlaceForm;
import com.guide.web.models.Place;
import com.guide.web.models.PlaceImage;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PlaceToPlaceFormConverter implements Converter<Place, PlaceForm> {
    @Override
    public PlaceForm convert(final Place place) {
        PlaceForm placeForm = new PlaceForm();
        placeForm.setId(place.getId());
        placeForm.setName(place.getName());
        placeForm.setDescription(place.getDescription());
        placeForm.setCity(place.getCity().getId());
        placeForm.setAddress(place.getAddress());
        placeForm.setWorkTime(place.getWorkTime());
        placeForm.setPhone(place.getPhone());
        placeForm.setSite(place.getSite());
        placeForm.setRegion(place.getCity().getRegion().getId());
        placeForm.setX(place.getMarker().getX());
        placeForm.setY(place.getMarker().getY());
        List<String> urls = new ArrayList<>();
        for (PlaceImage image : place.getImages()) {
            urls.add(image.getUrl());
        }
        placeForm.setPhoto(urls);
        return placeForm;
    }
}
