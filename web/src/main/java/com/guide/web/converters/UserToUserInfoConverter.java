package com.guide.web.converters;

import com.guide.web.dto.UserInfo;
import com.guide.web.models.User;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;

@Service
public class UserToUserInfoConverter implements Converter<User, UserInfo> {
    @Override
    public UserInfo convert(User user) {
        UserInfo userInfo = new UserInfo();
        userInfo.setId(user.getId());
        userInfo.setEmail(user.getEmail());
        userInfo.setName(user.getName());
        userInfo.setPassword(user.getPassword());
        userInfo.setRole(user.getRole());
        return userInfo;
    }
}
