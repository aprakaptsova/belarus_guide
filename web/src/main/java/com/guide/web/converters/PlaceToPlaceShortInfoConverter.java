package com.guide.web.converters;

import com.guide.web.dto.PlaceShortInfo;
import com.guide.web.models.Place;
import com.guide.web.models.PlaceImage;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class PlaceToPlaceShortInfoConverter implements Converter<Place, PlaceShortInfo> {
    @Override
    public PlaceShortInfo convert(final Place place) {
        PlaceShortInfo placeShortInfo = new PlaceShortInfo();
        placeShortInfo.setId(place.getId());
        placeShortInfo.setDescription(place.getDescription().split("\n")[0]);
        placeShortInfo.setName(place.getName());
        placeShortInfo.setCity(place.getCity().getName());
        List<String> urls = new ArrayList<>();
        for (PlaceImage image : place.getImages()) {
            urls.add(image.getUrl());
        }
        placeShortInfo.setPhoto(urls);
        return placeShortInfo;
    }
}
