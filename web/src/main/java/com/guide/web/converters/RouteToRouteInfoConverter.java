package com.guide.web.converters;

import com.guide.web.dto.PlaceInfo;
import com.guide.web.dto.RouteInfo;
import com.guide.web.models.Marker;
import com.guide.web.models.Place;
import com.guide.web.models.Route;
import com.guide.web.services.MarkerService;
import com.guide.web.services.PlaceService;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class RouteToRouteInfoConverter implements Converter<Route, RouteInfo> {
    private PlaceService placeService;

    public RouteToRouteInfoConverter(final PlaceService placeService) {
        this.placeService = placeService;
    }

    @Override
    public RouteInfo convert(Route route) {
        RouteInfo routeInfo = new RouteInfo();
        routeInfo.setId(route.getId());
        List<PlaceInfo> placesInfo = new ArrayList<>();
        List<Place> places = route.getPlaces();
        for (Place place : places) {
            placesInfo.add(placeService.getPlaceInfo(place.getId()));
        }
        routeInfo.setPlacesInfo(placesInfo);
        return routeInfo;
    }
}
