package com.guide.web.converters;

import com.guide.web.dto.CityForm;
import com.guide.web.models.City;
import com.guide.web.models.CityImage;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CityToCityFormConverter implements Converter<City, CityForm> {
    @Override
    public CityForm convert(final City city) {
        CityForm cityForm = new CityForm();
        cityForm.setId(city.getId());
        cityForm.setName(city.getName());
        cityForm.setDescription(city.getDescription());
        cityForm.setRegion(city.getRegion().getId());
        List<String> urls = new ArrayList<>();
        for (CityImage image : city.getImages()) {
            urls.add(image.getUrl());
        }
        cityForm.setPhoto(urls);
        return cityForm;
    }
}
