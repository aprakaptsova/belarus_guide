package com.guide.web.converters;

import com.guide.web.dto.CityForm;
import com.guide.web.models.City;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;

@Service
public class CityFormToCityConverter implements Converter<CityForm, City> {

    @Override
    public City convert(final CityForm cityForm) {
        City city = new City();
        city.setName(cityForm.getName());
        city.setDescription(cityForm.getDescription());
        return city;
    }
}
