package com.guide.web.converters;

import com.guide.web.dto.RegionForm;
import com.guide.web.models.Region;
import com.guide.web.models.RegionImage;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class RegionToRegionFormConverter implements Converter<Region, RegionForm> {

    @Override
    public RegionForm convert(final Region region) {
        RegionForm regionForm = new RegionForm();
        regionForm.setId(region.getId());
        regionForm.setName(region.getName());
        regionForm.setDescription(region.getDescription());
        List<String> urls = new ArrayList<>();
        for (RegionImage image : region.getImages()) {
            urls.add(image.getUrl());
        }
        regionForm.setPhoto(urls);
        return regionForm;
    }
}
