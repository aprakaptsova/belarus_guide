package com.guide.web.repo;

import com.guide.web.models.CityImage;
import org.springframework.data.repository.CrudRepository;

public interface CityImageRepository extends CrudRepository<CityImage, Long> {
}
