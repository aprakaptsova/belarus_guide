package com.guide.web.repo;

import com.guide.web.models.City;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface CityRepository extends CrudRepository<City, Long> {

    @Modifying
    @Transactional
    @Query(value = "delete from city_image where city_id=?1", nativeQuery = true)
    void deleteRegionImagesById(Long cityId);
}
