package com.guide.web.repo;

import com.guide.web.models.Region;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface RegionRepository extends CrudRepository<Region, Long> {

    @Modifying
    @Transactional
    @Query(value = "delete from region_image where region_id=?1", nativeQuery = true)
    void deleteRegionImagesById(Long regionId);

}
