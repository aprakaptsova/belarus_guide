package com.guide.web.repo;

import com.guide.web.models.RegionImage;
import org.springframework.data.repository.CrudRepository;

public interface RegionImageRepository extends CrudRepository<RegionImage, Long> {
}
