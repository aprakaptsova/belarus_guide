package com.guide.web.repo;

import com.guide.web.models.Place;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface PlaceRepository extends CrudRepository<Place, Long> {

    @Modifying
    @Transactional
    @Query(value = "delete from place_image where place_id=?1", nativeQuery = true)
    void deletePlaceImagesById(Long placeId);

    @Query(value = "select p from Place p where p.name like concat('%',lower(?1),'%')")
    List<Place> getPlacesByWord(String word) ;
}
