package com.guide.web.repo;

import com.guide.web.models.PlaceImage;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PlaceImageRepository extends CrudRepository<PlaceImage, Long> {
}
