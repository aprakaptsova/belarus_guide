package com.guide.web.repo;

import com.guide.web.models.Place;
import com.guide.web.models.Route;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RouteRepository extends CrudRepository<Route, Long> {
    @Query(value = "select r from Route r where r.user.id=?1")
    List<Route> getRoutesByUserId(Long userId);
}
