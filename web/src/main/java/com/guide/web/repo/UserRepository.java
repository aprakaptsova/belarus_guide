package com.guide.web.repo;

import com.guide.web.models.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {

    @Query(value = "select u from User u where u.email like concat('%',?1,'%')")
    User getUserByEmail(String email);

}
