package com.guide.web.controllers;

import com.guide.web.dto.UserForm;
import com.guide.web.services.UserService;
import com.guide.web.validators.CaptchaValidator;
import com.guide.web.validators.UserFormValidator;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class RegistrationController {

    private UserService userService;
    private UserFormValidator userFormValidator;
    private CaptchaValidator captchaValidator;

    public RegistrationController(final UserService userService,
                                  final UserFormValidator userFormValidator,
                                  final CaptchaValidator captchaValidator) {
        this.userService = userService;
        this.userFormValidator = userFormValidator;
        this.captchaValidator = captchaValidator;
    }

    @GetMapping("/registration")
    public String newUser(Model model) {
        model.addAttribute(new UserForm());
        return "registration";
    }

    @PostMapping("/registration")
    public String addNewUser(@Valid UserForm userForm, BindingResult bindingResult, Model model) {
        userFormValidator.validate(userForm, bindingResult);
        if (!userForm.getEmail().equals("") && userService.getUserByEmail(userForm.getEmail()) != null) {
            if (!bindingResult.hasFieldErrors("email")) {
                bindingResult.rejectValue("email", "duplicate_email", "Данный адрес уже зарегистрирован");
            }
        }
        if (bindingResult.hasErrors()) {
            return "registration";
        }
        final boolean isValidCaptcha = captchaValidator.validateCaptcha(userForm.getCaptchaResponse());
        if (!isValidCaptcha) {
            model.addAttribute("captcha_error", "Пожалуйста, пройдите проверку");
            return "registration";
        }
        userService.add(userForm);
        return "redirect:/login";
    }
}
