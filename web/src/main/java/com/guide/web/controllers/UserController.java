package com.guide.web.controllers;

import com.guide.web.dto.RouteInfo;
import com.guide.web.dto.UserInfo;
import com.guide.web.models.User;
import com.guide.web.repo.UserRepository;
import com.guide.web.services.RouteService;
import com.guide.web.services.UserService;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.Collections;
import java.util.List;

@Controller
@RequestMapping("/user")
public class UserController {
    private UserService userService;
    private UserRepository userRepository;
    private RouteService routeService;

    public UserController(final UserService userService,
                          final UserRepository userRepository,
                          final RouteService routeService) {
        this.userService = userService;
        this.userRepository = userRepository;
        this.routeService = routeService;
    }

    @GetMapping("/routes")
    public String getUserRoutes(@AuthenticationPrincipal UserDetails userDetails,
                                Model model){
        User user = userService.getUserByEmail(userDetails.getUsername());
        UserInfo userInfo = userService.getUserInfo(user.getId());
        model.addAttribute("user", userInfo);
        List<RouteInfo> routesInfo = userInfo.getRoutes();
        Collections.reverse(routesInfo);
        model.addAttribute("routes", routesInfo);
        return "userRoutes";
    }

    @RequestMapping(value = "/getUserRoutes")
    @ResponseBody
    public List<RouteInfo> getUserRoutesData(@AuthenticationPrincipal UserDetails userDetails) {
        User user = userService.getUserByEmail(userDetails.getUsername());
        List<RouteInfo> routesInfo = routeService.getRoutesInfoByUserId(user.getId());
        return  routesInfo;
    }

    @RequestMapping(value = "/deleteUserRoute")
    @ResponseBody
    public void deleteUserRoute(@AuthenticationPrincipal UserDetails userDetails,
                                @RequestBody long routeId) {
        List<RouteInfo> routesInfo = routeService.getRoutesInfoByUserId(userService.getUserByEmail(userDetails.getUsername()).getId());
        RouteInfo routeInfo = routeService.getRouteInfo(routeId);
        if(routesInfo.stream().anyMatch(route -> route.getId() == routeInfo.getId())) {
            routeService.deleteRoute(routeId);
        }

        //return new ModelAndView("userRoutes");
    }
}
