package com.guide.web.controllers;

import com.guide.web.dto.CityForm;
import com.guide.web.dto.CityInfo;
import com.guide.web.repo.CityRepository;
import com.guide.web.services.CityService;
import com.guide.web.services.PlaceService;
import com.guide.web.services.RegionService;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.Valid;
import java.util.List;

@Controller
public class CitiesController {

    private CityRepository cityRepository;
    private RegionService regionService;
    private CityService cityService;
    private PlaceService placeService;

    public CitiesController(final CityRepository cityRepository,
                            final RegionService regionService,
                            final CityService cityService,
                            final PlaceService placeService) {
        this.cityRepository = cityRepository;
        this.regionService = regionService;
        this.cityService = cityService;
        this.placeService = placeService;
    }

    @GetMapping("/cities/{cityId}")
    public String citiesInfo(@PathVariable(value = "cityId") long cityId,
                             Model model,
                             @AuthenticationPrincipal UserDetails user) {
        model.addAttribute("places", placeService.getPlacesByCity(cityId));
        if (user != null && user.getAuthorities().contains(new SimpleGrantedAuthority("ROLE_ADMIN"))) {
            model.addAttribute("admin", true);
        }
        model.addAttribute("city", cityService.getCityInfo(cityId));
        model.addAttribute("region", regionService.getRegionShortInfo(cityService.get(cityId).getRegion().getId()));
        return "placesList";
    }

    @GetMapping("/admin/cities/add/{regionId}")
    public String addCityForm(@PathVariable("regionId") long regionId,  Model model) {
        model.addAttribute("regions", regionService.getAllShortInfo());
        CityForm cityForm = new CityForm();
        cityForm.setRegion(regionId);
        model.addAttribute(cityForm);
        return "cityAdding";
    }

    @PostMapping("/admin/cities/add/{regionId}")
    public String addCity(@PathVariable("regionId") long regionId,
                          @Valid CityForm cityForm, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("regions", regionService.getAllShortInfo());
            return "cityAdding";
        }
        cityService.add(cityForm);
        return "redirect:/regions/" + cityForm.getRegion();
    }

    @GetMapping(path = "/getCitiesByRegion/{id}")
    @ResponseBody
    public List<CityInfo> getPublishingTopics(@PathVariable final Long id) {
        return cityService.getCitiesByRegion(id);
    }

    @GetMapping("admin/cities/edit/{cityId}")
    public String editRegion(@PathVariable(value = "cityId") long cityId, Model model) {
        model.addAttribute("regions", regionService.getAllShortInfo());
        model.addAttribute(cityService.getEditForm(cityId));
        return "cityAdding";

    }

    @PostMapping("admin/cities/edit/{cityId}")
    public String saveEditCity(@PathVariable(value = "cityId") long cityId,
                               @Valid CityForm cityForm, BindingResult bindingResult,
                               Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("regions", regionService.getAllShortInfo());
            return "cityAdding";
        }
        cityService.update(cityId, cityForm);
        return "redirect:/regions/" + cityForm.getRegion();
    }
}
