package com.guide.web.controllers;

import com.guide.web.converters.PlaceToPlaceInfoConverter;
import com.guide.web.dto.PlaceInfo;
import com.guide.web.repo.PlaceRepository;
import com.guide.web.services.PlaceService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

@RequestMapping
@RestController
public class MapController {

    private PlaceService placeService;

    private PlaceRepository placesRepository;

    public MapController(final PlaceRepository placesRepository,
                         final PlaceService placeService) {
        this.placesRepository = placesRepository;
        this.placeService = placeService;
    }

    @GetMapping("/map")
    public ModelAndView map() {
        return new ModelAndView("map");
    }

    @RequestMapping(value = "/ajaxplaces", method = RequestMethod.GET)
    public List<PlaceInfo> ajaxMarkers() {
        List<PlaceInfo> placesInfo = new ArrayList<>();
        placesRepository.findAll().forEach(place -> placesInfo.add(placeService.getPlaceInfo(place.getId())));
        return placesInfo;
    }
}

