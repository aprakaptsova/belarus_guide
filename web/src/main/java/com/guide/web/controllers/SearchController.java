package com.guide.web.controllers;

import com.guide.web.services.PlaceService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class SearchController {

    private PlaceService placeService;

    public SearchController(final PlaceService placeService) {
        this.placeService = placeService;
    }

    @GetMapping("/search")
    public String searchPlace(@RequestParam("param") String word, Model model) {
        model.addAttribute("places", placeService.getPlacesByWord(word));
        return "placesList";
    }
}
