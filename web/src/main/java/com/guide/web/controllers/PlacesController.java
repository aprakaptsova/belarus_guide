package com.guide.web.controllers;

import com.guide.web.dto.PlaceForm;
import com.guide.web.services.CityService;
import com.guide.web.services.PlaceService;
import com.guide.web.services.RegionService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class PlacesController {

    private RegionService regionService;
    private PlaceService placeService;
    private CityService cityService;

    public PlacesController(final RegionService regionService,
                            final PlaceService placeService,
                            final CityService cityService) {
        this.regionService = regionService;
        this.placeService = placeService;
        this.cityService = cityService;
    }

    @GetMapping("/places/{placeId}")
    public String placesInfo(@PathVariable(value = "placeId") long placeId, Model model) {
        model.addAttribute("place", placeService.getPlaceInfo(placeId));
        return "place";
    }

    @GetMapping("/admin/places/add/{regionId}/{cityId}")
    public String addPlaceForm(@PathVariable("regionId") long regionId,
                               @PathVariable("cityId") long cityId,
                               Model model) {
        model.addAttribute("regions", regionService.getAllShortInfo());
        model.addAttribute("cities", cityService.getCitiesByRegion(regionId));
        PlaceForm placeForm = new PlaceForm();
        placeForm.setRegion(regionService.get(regionId).getId());
        placeForm.setCity(cityService.get(cityId).getId());
        model.addAttribute(placeForm);
        return "placeAdding";
    }

    @PostMapping("/admin/places/add/{regionId}/{cityId}")
    public String addPlace(@PathVariable("regionId") long regionId,
                           @PathVariable("cityId") long cityId,
                           @Valid PlaceForm placeForm, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("regions", regionService.getAllShortInfo());
            model.addAttribute("cities", cityService.getCitiesByRegion(placeForm.getRegion()));
            return "placeAdding";
        }
        placeService.add(placeForm);
        return "redirect:/cities/" + placeForm.getCity();
    }

    @GetMapping("admin/places/edit/{placeId}")
    public String editRegion(@PathVariable(value = "placeId") long placeId, Model model) {
        PlaceForm placeForm = placeService.getEditForm(placeId);
        model.addAttribute("regions", regionService.getAllShortInfo());
        model.addAttribute("cities", cityService.getCitiesByRegion(placeForm.getRegion()));
        model.addAttribute(placeService.getEditForm(placeId));
        model.addAttribute(placeForm);
        return "placeAdding";

    }

    @PostMapping("admin/places/edit/{placeId}")
    public String saveEditPlace(@PathVariable(value = "placeId") long placeId,
                                @Valid PlaceForm placeForm, BindingResult bindingResult,
                                Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("regions", regionService.getAllShortInfo());
            model.addAttribute("cities", cityService.getCitiesByRegion(placeForm.getRegion()));
            return "placeAdding";
        }
        placeService.update(placeId, placeForm);
        return "redirect:/cities/" + placeForm.getCity();
    }
}
