package com.guide.web.controllers;

import com.guide.web.converters.PlaceToPlaceInfoConverter;
import com.guide.web.dto.PlaceInfo;
import com.guide.web.dto.RouteInfo;
import com.guide.web.models.Marker;
import com.guide.web.models.Place;
import com.guide.web.models.Route;
import com.guide.web.models.User;
import com.guide.web.repo.MarkerRepository;
import com.guide.web.repo.PlaceRepository;
import com.guide.web.repo.UserRepository;
import com.guide.web.services.PlaceService;
import com.guide.web.services.RouteService;
import com.guide.web.services.UserService;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

@RequestMapping
@RestController
public class DirectionController {

    private PlaceRepository placeRepository;
    private PlaceService placeService;
    private RouteService routeService;
    private UserService userService;

    private List<PlaceInfo> placesInfo;

    public DirectionController(final PlaceRepository placeRepository,
                               final PlaceService placeService,
                               final RouteService routeService,
                               final UserService userService) {
        this.placeRepository = placeRepository;
        this.placeService = placeService;
        this.routeService = routeService;
        this.userService = userService;
    }

    @RequestMapping(value = "/ajaxdirection")
    public List<PlaceInfo> getPlacesInfoByBalloons(@RequestBody(required = false) List<Long> chosenBalloons) {
        if (chosenBalloons != null) {
            placesInfo = new ArrayList<>();
            placeRepository.findAllById(chosenBalloons).forEach(place -> placesInfo.add(placeService.getPlaceInfo(place.getId())));
        }
        return placesInfo;
    }

    @GetMapping(value = "/getdirections")
    @ResponseBody
    public ModelAndView getRoute(@AuthenticationPrincipal UserDetails user,
                                  Model model) {//, HttpServletRequest request, HttpServletResponse response){//@RequestBody(required = false) List<String> chosenBalloons) {
        ModelAndView mav = new ModelAndView("makeDirections");
        boolean userAuth = false;
        if (user != null) {
            userAuth = true;
        }
        mav.addObject("userAuth", userAuth);
        return mav;
    }

    @RequestMapping(value = "/savedirection")
    public void saveDirection(@RequestBody(required = false) List<Long> chosenBalloons,
                              @AuthenticationPrincipal UserDetails user) {
        routeService.saveRoute(chosenBalloons, user);
    }

    @RequestMapping(value = "/updateroute")
    public void updateRoute(@RequestBody(required = false) RouteInfo routeInfo) {
        routeService.updateRoute(routeInfo);
    }

    @GetMapping("user/direction/{directionId}")
    public ModelAndView getUserRouteForHTML(@PathVariable(value = "directionId") long directionId,
                                            @AuthenticationPrincipal UserDetails userd) {
        Route route = routeService.get(directionId);
        User user = userService.getUserByEmail(userd.getUsername());
        if(route.getUser().getId() != user.getId()) {
            return new ModelAndView("error/403");
        }
        ModelAndView modelAndView = new ModelAndView("userDirection");
        modelAndView.addObject("route", routeService.getRouteInfo(directionId));
        return modelAndView;
    }

    @GetMapping(path = "/ajaxdirection/{directionId}")
    public RouteInfo getUserRoute(@PathVariable(value = "directionId") long directionId) {
        return routeService.getRouteInfo(directionId);
    }

    //method = RequestMethod.POST)
    //headers = {"Content-type=application/json"})


    //return chosenBalloons;
        /*for(int i = 0; i < chosenBalloons.stream().count(); i++){
            System.out.println(chosenBalloons.get(i));
        }*/
    //List<String> choosenBaloons = (List<String>)request.getAttribute("data");


    /*@RequestMapping(value = "/getdirections", method = RequestMethod.POST)
    public @ResponseBody
    ModelAndView saveOrder(@RequestParam(value="chosenBalloons[]") String[] chosenBalloons){
        return new ModelAndView("makeDirections", "markersId", chosenBalloons);
    }*/

}
