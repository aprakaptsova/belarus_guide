package com.guide.web.controllers;

import com.guide.web.dto.RegionForm;
import com.guide.web.services.CityService;
import com.guide.web.services.RegionService;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.List;

@Controller
public class RegionController {

    private final RegionService regionService;
    private final CityService cityService;

    public RegionController(final RegionService regionService,
                            final CityService cityService) {
        this.regionService = regionService;
        this.cityService = cityService;
    }

    @GetMapping("/regions")
    public String regionsMain(Model model, @AuthenticationPrincipal UserDetails user) {
        List<RegionForm> regions = regionService.getAll();
        model.addAttribute("regions", regionService.getAll());
        if (user != null && user.getAuthorities().contains(new SimpleGrantedAuthority("ROLE_ADMIN"))) {
            model.addAttribute("admin", true);
        }
        return "regionsList";
    }

    @GetMapping("/regions/{regionId}")
    public String regionsInfo(@PathVariable(value = "regionId") long regionId,
                              Model model,
                              @AuthenticationPrincipal UserDetails user) {
        model.addAttribute("cities", cityService.getCitiesByRegion(regionId));
        if (user != null && user.getAuthorities().contains(new SimpleGrantedAuthority("ROLE_ADMIN"))) {
            model.addAttribute("admin", true);
        }
        model.addAttribute("region", regionService.getRegionShortInfo(regionId));
        return "citiesList";
    }

    @GetMapping("admin/regions/add")
    public String newRegion(Model model) {
        model.addAttribute(new RegionForm());
        return "regionAdding";
    }

    @PostMapping("admin/regions/add")
    public String addRegion(@Valid RegionForm regionForm, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "regionAdding";
        }
        regionService.add(regionForm);
        return "redirect:/regions";
    }

    @GetMapping("admin/regions/edit/{regionId}")
    public String editRegion(@PathVariable(value = "regionId") long regionId, Model model) {
        model.addAttribute(regionService.getEditForm(regionId));
        return "regionAdding";

    }

    @PostMapping("admin/regions/edit/{regionId}")
    public String saveEditRegion(@PathVariable(value = "regionId") long regionId,
                                 @Valid RegionForm regionForm, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "regionAdding";
        }
        regionService.update(regionId, regionForm);
        return "redirect:/regions";
    }
}

