package com.guide.web.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

public class RegionForm {
    private long id;
    @NotBlank(message = "Поле должно быть заполнено")
    private String name;
    @NotBlank(message = "Поле должно быть заполнено")
    private String description;
    @NotNull(message = "Загрузите хотя бы 1 фотографию")
    private List<String> photo;

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public List<String> getPhoto() {
        return photo;
    }

    public void setPhoto(final List<String> photo) {
        this.photo = photo;
    }

    public long getId() {
        return id;
    }

    public void setId(final long id) {
        this.id = id;
    }
}
