package com.guide.web.dto;

import java.util.List;

public class PlaceInfo {

    private long id;
    private String name;
    private String city;
    private String region;
    private List<String> description;
    private String address;
    private String site;
    private String phone;
    private String workTime;
    private MarkerInfo markerInfo;
    private List<String> photo;

    public long getId() {
        return id;
    }

    public void setId(final long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(final String city) {
        this.city = city;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(final String region) {
        this.region = region;
    }

    public List<String> getDescription() {
        return description;
    }

    public void setDescription(final List<String> description) {
        this.description = description;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(final String address) {
        this.address = address;
    }

    public String getSite() {
        return site;
    }

    public void setSite(final String site) {
        this.site = site;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(final String phone) {
        this.phone = phone;
    }

    public String getWorkTime() {
        return workTime;
    }

    public void setWorkTime(final String workTime) {
        this.workTime = workTime;
    }

    public MarkerInfo getMarkerInfo() {
        return markerInfo;
    }

    public void setMarkerInfo(final MarkerInfo markerInfo) {
        this.markerInfo = markerInfo;
    }

    public List<String> getPhoto() {
        return photo;
    }

    public void setPhoto(final List<String> photo) {
        this.photo = photo;
    }
}
