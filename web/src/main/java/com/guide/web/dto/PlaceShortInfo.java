package com.guide.web.dto;

import java.util.List;

public class PlaceShortInfo {
    private long id;
    private String city;
    private String name;
    private String description;
    private List<String> photo;

    public long getId() {
        return id;
    }

    public void setId(final long id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(final String city) {
        this.city = city;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public List<String> getPhoto() {
        return photo;
    }

    public void setPhoto(final List<String> photo) {
        this.photo = photo;
    }
}
