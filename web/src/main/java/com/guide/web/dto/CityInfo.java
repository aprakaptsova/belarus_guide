package com.guide.web.dto;

import java.util.List;

public class CityInfo {
    private long id;
    private String region;
    private String name;
    private String description;
    private List<String> photo;

    public String getRegion() {
        return region;
    }

    public void setRegion(final String region) {
        this.region = region;
    }

    public long getId() {
        return id;
    }

    public void setId(final long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public List<String> getPhoto() {
        return photo;
    }

    public void setPhoto(final List<String> photo) {
        this.photo = photo;
    }
}
