package com.guide.web.dto;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

public class PlaceForm {

    private long id;
    @NotBlank(message = "Поле должно быть заполнено")
    private String name;
    @Min(value = 1, message = "Поле должно быть заполнено")
    private long city;
    @Min(value = 1, message = "Поле должно быть заполнено")
    private long region;
    @NotBlank(message = "Поле должно быть заполнено")
    private String description;
    @NotBlank(message = "Поле должно быть заполнено")
    private String address;
    private String site;
    private String phone;
    private String workTime;
    @NotBlank(message = "Укажите местоположение достопримечательности")
    private String x;
    private String y;
    private int zoom;
    private String centerX;
    private String centerY;
    @NotNull(message = "Загрузите хотя бы 1 фотографию")
    private List<String> photo;

    public long getId() {
        return id;
    }

    public void setId(final long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(final String address) {
        this.address = address;
    }

    public String getSite() {
        return site;
    }

    public void setSite(final String site) {
        this.site = site;
    }

    public String getWorkTime() {
        return workTime;
    }

    public void setWorkTime(final String workTime) {
        this.workTime = workTime;
    }

    public long getCity() {
        return city;
    }

    public void setCity(final long city) {
        this.city = city;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(final String phone) {
        this.phone = phone;
    }

    public String getX() {
        return x;
    }

    public void setX(final String x) {
        this.x = x;
    }

    public String getY() {
        return y;
    }

    public void setY(final String y) {
        this.y = y;
    }

    public List<String> getPhoto() {
        return photo;
    }

    public void setPhoto(final List<String> photo) {
        this.photo = photo;
    }

    public long getRegion() {
        return region;
    }

    public void setRegion(final long region) {
        this.region = region;
    }

    public int getZoom() {
        return zoom;
    }

    public void setZoom(int zoom) {
        this.zoom = zoom;
    }

    public String getCenterX() {
        return centerX;
    }

    public void setCenterX(String centerX) {
        this.centerX = centerX;
    }

    public String getCenterY() {
        return centerY;
    }

    public void setCenterY(String centerY) {
        this.centerY = centerY;
    }
}
