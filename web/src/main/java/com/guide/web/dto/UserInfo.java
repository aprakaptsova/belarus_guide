package com.guide.web.dto;
import java.util.List;

public class UserInfo {
    private long id;
    private String name;
    private String email;
    private String password;
    private char role;
    List<RouteInfo> routes;

    public long getId() {
        return id;
    }

    public void setId(final long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(final String password) {
        this.password = password;
    }

    public char getRole() {
        return role;
    }

    public void setRole(final char role) {
        this.role = role;
    }

    public List<RouteInfo> getRoutes() {
        return routes;
    }

    public void setRoutes(final List<RouteInfo> routes) {
        this.routes = routes;
    }
}
