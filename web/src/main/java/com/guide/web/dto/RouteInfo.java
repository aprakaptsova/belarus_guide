package com.guide.web.dto;

import com.guide.web.models.Marker;
import com.guide.web.models.User;

import java.util.List;

public class RouteInfo {
    private long id;
    private List<PlaceInfo> placesInfo;

    public long getId() {
        return id;
    }

    public void setId(final long id) {
        this.id = id;
    }

    public List<PlaceInfo> getPlacesInfo() {
        return placesInfo;
    }

    public void setPlacesInfo(final List<PlaceInfo> placesInfo) {
        this.placesInfo = placesInfo;
    }
}
