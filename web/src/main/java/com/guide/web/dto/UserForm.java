package com.guide.web.dto;


import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

public class UserForm {

    private long id;
    @NotBlank(message = "Поле должно быть заполнено")
    private String name;
    @NotBlank(message = "Поле должно быть заполнено")
    @Email(message = "Введите существующий адрес")
    private String email;
    @Pattern(regexp = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{8,}$", message = "Минимум 8 символов, хотя бы 1 цифра и " +
            "заглавная буква")
    @NotBlank(message = "Поле должно быть заполнено")
    private String password;
    @NotBlank(message = "Поле должно быть заполнено")
    private String passwordCopy;
    private String captchaResponse;

    public String getCaptchaResponse() {
        return captchaResponse;
    }

    public void setCaptchaResponse(String captchaResponse) {
        this.captchaResponse = captchaResponse;
    }

    public long getId() {
        return id;
    }

    public void setId(final long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(final String password) {
        this.password = password;
    }

    public String getPasswordCopy() {
        return passwordCopy;
    }

    public void setPasswordCopy(final String passwordCopy) {
        this.passwordCopy = passwordCopy;
    }
}
