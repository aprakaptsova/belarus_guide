$(function () {
    $('.carousel-inner>.carousel-item:first-child').addClass('active');

    $('.carousel').carousel({
        interval: 3000,
        keyboard: false,
        pause: 'hover',
        ride: 'carousel',
        wrap: true
    });
});