var route, places = [];
var myMap, directionCoords;
var changes = false;

$(document).ready(function () {
    let routeId = $('#route-id').val();

    $.get('/ajaxdirection/' + routeId, function (data) {
        if (data) {
            route = data;
            places = route.placesInfo;
        }

        drawShortPlaces();

        ymaps.ready(init);

        $('#save-btn').on('click', updateRoute);
    });

});

function updateRoute() {
    route.placesInfo = places;
    $.ajax({
        type: 'POST',
        url: '/updateroute',
        data: JSON.stringify(route),
        success: function () {
            makeSaveInactive();
            $.jGrowl('Маршрут сохранен');
        },
        contentType: 'application/json',
    });
}

function init() {
    myMap = new ymaps.Map("map", {
        center: [53.54, 27.33],
        zoom: 7
    }), directionCoords = [];

    for (let i = 0; i < places.length; i++) {
        directionCoords.push([places[i].markerInfo.x, places[i].markerInfo.y]);
    }

    var multiRoute = new ymaps.multiRouter.MultiRoute({
        // Точки маршрута. Точки могут быть заданы как координатами, так и адресом.
        referencePoints: directionCoords,
    }, {
        // Автоматически устанавливать границы карты так,
        // чтобы маршрут был виден целиком.
        boundsAutoApply: true
    });

    myMap.geoObjects.add(multiRoute);
};

function drawShortPlaces() {
    places.forEach(place => {
        let placeElem = $('.example .short-place').clone();

        let img = placeElem.find('.place-img');
        img.attr('src', place.photo[0]);

        let a = placeElem.find('a');
        a.text(place.name);
        a.attr('href', a.attr('href') + place.id);

        let description = placeElem.find('.description');
        description.text(place.description);

        let addresAndTime = placeElem.find('.address-time');
        addresAndTime.text('Адрес: ' + place.address + (place.workTime ? ', Время работы: ' + place.workTime : ""));

        let trash = placeElem.find('.delete-button');
        trashButtonClick(trash, placeElem, place);

        let upPlace = placeElem.find('.up');
        upArrowClick(upPlace, place);

        let downPlace = placeElem.find('.down');
        downArrowClick(downPlace, place);

        $('body .short-places').append(placeElem);
    });
}

function trashButtonClick(trash, removeElem, place) {
    trash.click(function () {
        if (!changes) {
            makeSaveActive();
        }

        let index = getBalloonsFromPlaces().indexOf(place.id);

        places.splice(index, 1);

        drawDirectionOnMap();

        removeElem.remove();
    });
}

function upArrowClick(upArrow, place) {
    upArrow.click(function () {
        if (!changes) {
            makeSaveActive();
        }

        let ballons = getBalloonsFromPlaces();
        ballons = swapBack(ballons, ballons.indexOf(place.id));

        changePage(ballons);
    });
}

function downArrowClick(downArrow, place) {
    downArrow.click(function () {
        if (!changes) {
            makeSaveActive();
        }

        let ballons = getBalloonsFromPlaces(places);
        ballons = swapForward(ballons, ballons.indexOf(place.id));

        changePage(ballons);
    });
}

function makeSaveActive() {
    changes = true;
    $('#save-btn').show();
}

function makeSaveInactive() {
    changes = false;
    $('#save-btn').hide();
}

function swapBack(arr, index) {
    let el = arr[index - 1];
    arr[index - 1] = arr[index];
    arr[index] = el;
    return arr;
}

function swapForward(arr, index) {
    let el = arr[index + 1];
    arr[index + 1] = arr[index];
    arr[index] = el;
    return arr;
}

function drawDirectionOnMap() {
    removeAllPlacesOnMap();

    for (let i = 0; i < places.length; i++) {
        directionCoords.push([places[i].markerInfo.x, places[i].markerInfo.y]);
    }
    var multiRoute = new ymaps.multiRouter.MultiRoute({
        referencePoints: directionCoords,
    }, {
        boundsAutoApply: true
    });
    myMap.geoObjects.add(multiRoute);
}

function changePage(chosenBalloons) {
    $('.short-places').empty();
    places = filterPlaces(chosenBalloons);
    drawDirectionOnMap();
    drawShortPlaces();
}

function filterPlaces(ballons) {
    let newPlaces = [];
    for (let i = 0; i < ballons.length; i++) {
        let place = getPlaceByBallon(ballons[i]);
        newPlaces.push(place);
    }
    return newPlaces;
}

function getPlaceByBallon(balloon) {
    return places.filter(elem => elem.id === balloon)[0];
}

function removeAllPlacesOnMap() {
    if (myMap) {
        myMap.geoObjects.removeAll();
        directionCoords = [];
    }
}

function getBalloonsFromPlaces() {
    ballons = [];
    places.filter(place => ballons.push(place.id));
    return ballons;
}

function getPlaceIndex(placeId) {
    return places.map(place => place.id).indexOf(placeId);
}