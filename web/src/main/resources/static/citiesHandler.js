$(document).ready(function () {
    var $citiesSelect = $('#cities');
    var $regionsSelect = $('#regions');

    $regionsSelect.on('change', onRegionChange);

    function onRegionChange(event) {
        var regionId = event.target.value;

        clearCities();
        fetchData(regionId);
    };

    function fetchData(regionId) {
        fetch("/getCitiesByRegion/" + regionId)
            .then(resp => resp.json())
            .then(function (cities) {
                $citiesSelect.removeAttr('disabled');
                $('<option hidden value="-1">').appendTo($citiesSelect);
                cities.forEach(function (r) {
                    $('<option>').val(r.id).text(r.name).appendTo($citiesSelect);
                });
            });
    }

    function clearCities() {
        $citiesSelect.find('option').remove();
    }
});