var places;
var chosenBalloons = [];

$(document).ready(function () {


    $.ajax({
        url: '/ajaxplaces',
        type: 'GET',
        success: function (data) {
            places = data;
            ymaps.ready(init(places));
        },
        error: function (data, status) {
            console.log(status); //or whatever
        }
    });


    /*$.get("/ajaxplaces", function (data, status) {
        places = data;
        ymaps.ready(init(places));
    }).fail(function (status) { alert(status) });*/
});

function init(places) {
    // Создание карты.
    return function () {
        var myMap = new ymaps.Map("map", {
            // Координаты центра карты.
            // Порядок по умолчанию: «широта, долгота».
            // Чтобы не определять координаты центра карты вручную,
            // воспользуйтесь инструментом Определение координат.
            center: [53.54, 27.33],
            // Уровень масштабирования. Допустимые значения:
            // от 0 (весь мир) до 19.d
            zoom: 7
        }), myPlacemarkCollection = new ymaps.GeoObjectCollection(),
            lastOpenedBalloon = false;

        //let placeMark = new ymaps.Placemark(myMap.getCenter(),{baloonContent = 'Минск город герой'});

        for (let i = 0; i < places.length; i++) {
            let myPlacemark = new ymaps.Placemark([places[i].markerInfo.x, places[i].markerInfo.y], {
                balloonContentHeader: '<a href="/places/' + places[i].id + '" target="_blank">' + places[i].name + '</a>',
                balloonContentBody: '<img src="' + places[i].photo[0] + '" style="width:100px; height: auto; float: left;"/>' + places[i].address,
                balloonContentFooter: '<div style="max-height: 60px;">' + places[i].description[0] + '</div>',
                hintContent: places[i].name,
                myId: places[i].id,
            });

            /*var myPlacemark = new ymaps.Placemark([54.897225, 29.568142], {
                // Чтобы балун и хинт открывались на метке, необходимо задать ей определенные свойства.
                balloonContentHeader: "Термин метки",
                balloonContentBody: "Описание <em>метки</em>",
                balloonContentFooter: "Подпись метки",
                hintContent: "Надпись при наведении",
                myId: 'first',
            });*/

            myPlacemarkCollection.add(myPlacemark);
        }
        myMap.geoObjects.add(myPlacemarkCollection);
        // Установка центра и масштаба карты таким образом, чтобы вся коллекция была видна.
        myMap.setBounds(myPlacemarkCollection.getBounds());

        // Обработка событий карты:
        // - boundschange - изменение границ области показа;
        // - type - изменение типа карты;
        // - balloonclose - закрытие балуна.
        myMap.events.add(['boundschange', 'typechange', 'balloonclose'], setLocationHash);

        // Обработка событий открытия балуна для любого элемента
        // коллекции.
        // В данном случае на карте находятся только метки одной коллекции.
        // Чтобы обработать события любых геообъектов карты можно использовать
        // myMap.geoObjects.events.add(['balloonopen'],function (e) { ...


        $('#addPlace').on('click', function () {
            //alert(object.geometry.getCoordinates());
            if (lastOpenedBalloon != false) {
                chosenBalloons.push(lastOpenedBalloon);
                let lengthBalloons = chosenBalloons.length;
                filterChosenBalloons();
                if (lengthBalloons > chosenBalloons.length) {
                    $.jGrowl('Достопримечательность уже добавлена в поездку');
                }
                else {
                    localStorage.setItem("chosenBalloons", JSON.stringify(chosenBalloons));
                    sendPostBalloons();
                    setSaveActive();
                    $.jGrowl('Достопримечательность добавлена в поездку');
                }
                //alert('достопримечательность добвлена в поездку' + chosenBalloons);
            }
            else {
                $.jGrowl('Метка не выбрана');
                //alert('Метка не выбрана');
            }
        });

        $('#getDirections').on('click', function () {
            //$http.post("/getdirections", chosenBalloons);
            /*$.ajax({
                type: 'POST',
                dataType: 'json',
                contentType: 'application/json',
                url: '/getdirections',
                data: JSON.stringify(chosenBalloons),
                success: function () {
                    alert('Данные отправлены');
                },
            });*/
            //chosenBalloons = JSON.parse(localStorage.getItem('chosenBalloons'));
            sendPostBalloons();
            goToDirection();
        });

        var clusterer = new ymaps.Clusterer({
            gridSize: 64,
            groupByCoordinates: false,
            hasBalloon: true,
            hasHint: true,
            margin: 10,
            maxZoom: 14,
            minClusterSize: 2,
            showInAlphabeticalOrder: false,
            viewportMargin: 128,
            zoomMargin: 128,
            clusterDisableClickZoom: false,
        });
        clusterer.add(myPlacemarkCollection.toArray());
        myMap.geoObjects.add(clusterer);


        clusterer.events.add(['balloonopen'], function (e) {
            lastOpenedBalloon = e.get('target').properties.get('myId');
            setLocationHash();
        });

        clusterer.events.add('click', function (e) {
            //myMap.setCenter(e.get('coords'));
            //myMap.setZoom(myMap.getZoom() + 2);
            //myMap.setBounds(clusterer.getBounds());
        })

        setMapStateByHash();

        // Получение значение параметра name из адресной строки
        // браузера.
        function getParam(name, location) {
            location = location || window.location.hash;
            var res = location.match(new RegExp('[#&]' + name + '=([^&]*)', 'i'));
            return (res && res[1] ? res[1] : false);
        }

        // Передача параметров, описывающих состояние карты,
        // в адресную строку браузера.
        function setLocationHash() {
            var params = [
                'type=' + myMap.getType().split('#')[1],
                'center=' + myMap.getCenter(),
                'zoom=' + myMap.getZoom()
            ];
            if (myMap.balloon.isOpen()) {
                params.push('open=' + lastOpenedBalloon);
            }
            else {
                lastOpenedBalloon = false;
            }
            window.location.hash = params.join('&');
        }

        // Установка состояния карты в соответствии с переданными в адресной строке
        // браузера параметрами.
        function setMapStateByHash() {
            var hashType = getParam('type'),
                hashCenter = getParam('center'),
                hashZoom = getParam('zoom'),
                open = getParam('open');
            if (hashType) {
                myMap.setType('yandex#' + hashType);
            }
            if (hashCenter) {
                myMap.setCenter(hashCenter.split(','));
            }
            if (hashZoom) {
                myMap.setZoom(hashZoom);
            }
            if (open) {
                myPlacemarkCollection.each(function (geoObj) {
                    var id = geoObj.properties.get('myId');
                    if (id == open) {
                        geoObj.balloon.open();
                    }
                });
            }
            if (localStorage.getItem('chosenBalloons')) {
                chosenBalloons = JSON.parse(localStorage.getItem('chosenBalloons'));
            }
        }
    }
}

function setSaveActive() {
    localStorage.setItem('isSave', JSON.stringify(false));
}

function sendPostBalloons() {
    $.ajax({
        type: 'POST',
        url: '/ajaxdirection',
        //method: 'POST',
        data: JSON.stringify(chosenBalloons),
        success: function (response, statusText, status) {
            console.log('Запрос успешно отправился, получаем ответ', response);
            localStorage.setItem('chosenBalloons', JSON.stringify(chosenBalloons)); //если метки не добавлялись, чтобы создать переменную
        },
        error: function (XHR) {
            console.log('Ошибка запроса', XHR);
        },
        //dataType: 'json',
        contentType: 'application/json'
    });
}

function goToDirection() {
    location.href = "/getdirections";
}

function filterChosenBalloons() {
    chosenBalloons = uniq_fast(chosenBalloons);
}

function uniq_fast(a) {
    var seen = {};
    var out = [];
    var len = a.length;
    var j = 0;
    for (var i = 0; i < len; i++) {
        var item = a[i];
        if (seen[item] !== 1) {
            seen[item] = 1;
            out[j++] = item;
        }
    }
    return out;
}

