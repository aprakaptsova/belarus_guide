$(document).ready(function () {
    let logout = document.querySelector('.logout');
    logout.addEventListener('click', function () {
        localStorage.clear();
    });


    $('.route .delete-button').on('click', function(e) {
        let routeId = e.target.closest('.delete-button').dataset.routeid;
        deleteRouteOnDB(routeId);
    });
});

function deleteRouteOnDB(routeId) {
    $.ajax({
        type: 'POST',
        url: '/user/deleteUserRoute',
        data: JSON.stringify(routeId),
        success: function () { location.href = '/user/routes' },
        contentType: 'application/json',
    });
}