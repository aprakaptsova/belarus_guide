let chosenBalloons = [];

$(document).ready(function () {
    if (localStorage.getItem('chosenBalloons')) {
        chosenBalloons = JSON.parse(localStorage.getItem('chosenBalloons'));
    }

    $('#addmarker').on('click', function () {
        chosenBalloons.push(Number($(this).attr('data-placeid')));
        let lengthBalloons = chosenBalloons.length;
        filterChosenBalloons();
        if(lengthBalloons > chosenBalloons.length){
            $.jGrowl('Достопримечательность уже добавлена в поездку');
        }
        else {
            localStorage.setItem('chosenBalloons', JSON.stringify(chosenBalloons));
            //alert('достопримечательность добвлена в поездку' + chosenBalloons);
    
            $.ajax({
                type: 'POST',
                url: '/ajaxdirection',
                //method: 'POST',
                data: JSON.stringify(chosenBalloons),
                success: function (response, statusText, status) {
                    console.log('Запрос успешно отправился, получаем ответ', response);
                    setSaveActive();
                    $.jGrowl('Достопримечательность добавлена в поездку');
                },
                error: function (XHR) {
                    console.log('Ошибка запроса', XHR);
                },
                //dataType: 'json',
                contentType: 'application/json'
            });
        }
        
    });
});

function setSaveActive() {
    localStorage.setItem('isSave', JSON.stringify(false));
}

function filterChosenBalloons() {
    chosenBalloons = uniq_fast(chosenBalloons);
}

function uniq_fast(a) {
    var seen = {};
    var out = [];
    var len = a.length;
    var j = 0;
    for (var i = 0; i < len; i++) {
        var item = a[i];
        if (seen[item] !== 1) {
            seen[item] = 1;
            out[j++] = item;
        }
    }
    return out;
}