$(function () {
    document.getElementById('photo-list').addEventListener('click', onDeletePhotoClick);
    document.getElementById('add-photo').addEventListener('click', onPhotoAddClick);

    function onDeletePhotoClick(event) {
        var target = event.target;
        while (target !== event.currentTarget && target !== null) {
            if (target.name === 'deletePhoto') {
                var liItem = event.target.closest(".list-group-item");
                deletePhotoItem(liItem);
            }
            target = target.parentNode;
        }
    }

    function onPhotoAddClick() {
        var adsInput = document.getElementById("photo");
        if (adsInput.value === "") {
            return;
        }

        addPhoto(adsInput.value);
        adsInput.value = "";
    }

    //////////////////////////////////////////////

    function deletePhotoItem(liElement) {
        liElement.remove();
    }

    function addPhoto(link) {
        let adsLiItem = createPhoto(link);

        var adsList = document.getElementById("photo-list");
        adsList.appendChild(adsLiItem);
    }

    function createPhoto(linkText) {
        var listItem = document.querySelector("#photoTemplate li").cloneNode(true);
        var link = listItem.querySelector('a');
        link.href = linkText;
        link.textContent = linkText;

        listItem.querySelector('input').value = linkText;

        return listItem;
    }
});