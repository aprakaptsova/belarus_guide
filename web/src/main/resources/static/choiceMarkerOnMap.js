var myMap, myPlacemark;


$(document).ready(function () {
    ymaps.ready(init);

    $('form #regions').on('change', function () {
        var reg = $(this).find('option:selected').attr('label');
        setMap($(this).find('option:selected').attr('label'), 8);
    });

    $('form #cities').on('change', function () {
        let cities = document.getElementById('cities').options;
        let city = cities[cities.selectedIndex].text;

        var address = getStringFromArray([$('form #regions option:selected').attr('label'), city]);
        setMap(address, 10);
    });

});

function init() {
    myPlacemark,
        myMap = new ymaps.Map('map', {
            center: [53.54, 27.33],
            zoom: 7
        }, {
            searchControlProvider: 'yandex#search'
        });
    if ($('form .x').val() && $('form .y').val()) {
        myPlacemark = createPlacemark([$('form .x').val(), $('form .y').val()]);
        getAddress(myPlacemark.geometry.getCoordinates());
        myMap.geoObjects.add(myPlacemark);
        if($('form .centerX').val()){
            myMap.setCenter([$('form .centerX').val(), $('form .centerY').val()]);
            myMap.setZoom($('form .zoom').val());
        }
        else{
            myMap.setCenter([$('form .x').val(), $('form .y').val()]);
            myMap.setZoom(14);
        }
    }
    else {
        setMap(getStringFromArray([$('form #regions option:selected').attr('label'), $('form #cities option:selected').attr('label')]), 10);
    }




    //setMapStateByHash();

    // Слушаем клик на карте.
    myMap.events.add('mousedown', function (e) {
        var coords = e.get('coords');

        // Если метка уже создана – просто передвигаем ее.
        if (myPlacemark) {
            myPlacemark.geometry.setCoordinates(coords);
        }
        // Если нет – создаем.
        else {
            myPlacemark = createPlacemark(coords);
            myMap.geoObjects.add(myPlacemark);
            // Слушаем событие окончания перетаскивания на метке.
            myPlacemark.events.add('dragend', function () {
                getAddress(myPlacemark.geometry.getCoordinates());
            });
        }
        getAddress(coords);
        $('form .x').val(coords[0]);
        $('form .y').val(coords[1]);
        setLocation();
    });

    myMap.events.add(['boundschange', 'typechange', 'balloonclose', 'balloonopen'], setLocation);
}

function setMap(address, zoom) {
    ymaps.geocode(address).then(function (res) {
        var coord = res.geoObjects.get(0).geometry.getCoordinates();

        myMap.setCenter(coord);
        myMap.setZoom(zoom, {
            duration: 500
        });
        //setCenter();
    });
}

function setLocation() {
    $('form .zoom').val(myMap.getZoom());
    $('form .centerX').val(myMap.getCenter()[0]);
    $('form .centerY').val(myMap.getCenter()[1]);
}

// Создание метки.
function createPlacemark(coords) {
    return new ymaps.Placemark(coords, {
        iconCaption: 'поиск...'
    }, {
        preset: 'islands#violetDotIconWithCaption',
        draggable: true
    });
}

// Определяем адрес по координатам (обратное геокодирование).
function getAddress(coords) {
    myPlacemark.properties.set('iconCaption', 'поиск...');
    ymaps.geocode(coords).then(function (res) {
        var firstGeoObject = res.geoObjects.get(0);

        myPlacemark.properties
            .set({
                // Формируем строку с данными об объекте.
                iconCaption: [
                    // Название населенного пункта или вышестоящее административно-территориальное образование.
                    firstGeoObject.getLocalities().length ? firstGeoObject.getLocalities() : firstGeoObject.getAdministrativeAreas(),
                    // Получаем путь до топонима, если метод вернул null, запрашиваем наименование здания.
                    firstGeoObject.getThoroughfare() || firstGeoObject.getPremise(),
                ].filter(Boolean).join(', '),
                // В качестве контента балуна задаем строку с адресом объекта.
                balloonContent: firstGeoObject.getAddressLine(), //+ " x: " + coords[0].toPrecision(6) + " y: " + coords[1].toPrecision(6),
            });
        $('input[name=address]').val(firstGeoObject.getAddressLine());

    });
}

function getStringFromArray(arr) {
    return arr.join(' ');
}

    // Получение значение параметра name из адресной строки
    // браузера.
/*function getParam(name, location) {
    location = location || window.location.hash;
    var res = location.match(new RegExp('[#&]' + name + '=([^&]*)', 'i'));
    return (res && res[1] ? res[1] : false);
}*/

    // Передача параметров, описывающих состояние карты,
    // в адресную строку браузера.
/*function setLocationHash() {
    var params = [
        'type=' + myMap.getType().split('#')[1],
        'center=' + myMap.getCenter(),
        'zoom=' + myMap.getZoom()
    ];
    if (myMap.balloon.isOpen()) {
        params.push('open=' + lastOpenedBalloon);
    }
    else {
        lastOpenedBalloon = false;
    }
    window.location.hash = params.join('&');
}*/

    // Установка состояния карты в соответствии с переданными в адресной строке
    // браузера параметрами.
/*function setMapStateByHash() {
    var hashType = getParam('type'),
        hashCenter = getParam('center'),
        hashZoom = getParam('zoom'),
        open = getParam('open');
    if (hashType) {
        myMap.setType('yandex#' + hashType);
    }
    if (hashCenter) {
        myMap.setCenter(hashCenter.split(','));
    }
    if (hashZoom) {
        myMap.setZoom(hashZoom);
    }
    if (open) {
        myPlacemarkCollection.each(function (geoObj) {
            var id = geoObj.properties.get('myId');
            if (id == open) {
                geoObj.balloon.open();
            }
        });
    }
    if (localStorage.getItem('chosenBalloons')) {
        chosenBalloons = JSON.parse(localStorage.getItem('chosenBalloons'));
    }
}*/
