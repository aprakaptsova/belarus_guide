function validateAndGetCaptchaResponse() {
    const response = grecaptcha.getResponse();
    return response.length === 0 ? null : response;
}

$(document).ready(function () {
    $("#submit").click(function () {

        let captchaResponse = validateAndGetCaptchaResponse();
        if (captchaResponse) {
            $("#response").val(captchaResponse);
        }
    });
});
