let places = [];
var myMap, myPlacemarkCollection, directionCoords, chosenBalloons;

$(document).ready(function () {
    $.get("/ajaxdirection", function (data, status) {
        if (data) {
            chosenBalloons = JSON.parse(localStorage.getItem('chosenBalloons'));
            if (!chosenBalloons) {
                chosenBalloons = []; //если отправились данные меток но не записалось в локалсторэйдж
            }
            places = filterPlaces(data, chosenBalloons);
            localStorage.setItem('directionPlaces', JSON.stringify(places));
        }
        else if (localStorage.getItem('directionPlaces')) {
            places = JSON.parse(localStorage.getItem('directionPlaces'));
            chosenBalloons = JSON.parse(localStorage.getItem('chosenBalloons'));
        }

        drawShortPlaces(places);

        ymaps.ready(init(places));

        console.log(chosenBalloons);
    });

    $('#clear').on('click', function () {
        //removeAllPlaces();
        postPlaces();
        chosenBalloons = [];
        places = [];
        setSaveBtnEnabled();
    });

    let isSave = JSON.parse(localStorage.getItem('isSave'));

    if (isSave) {
        setSaveBtnDisabled();
    }


    $('#save').on('click', function () {
        if ($(this).hasClass('disabled')) {
            $.jGrowl('Маршрут уже сохранен');
        }
        else {
            if (chosenBalloons.length !== 0) {
                saveDirection(chosenBalloons);
                setSaveBtnDisabled();
            }
            else {
                $.jGrowl('Маршрут не сохранен, нет ни одной метки');
            }
        }
    });
});

function setSaveBtnDisabled() {
    $('#save').addClass('disabled');
    localStorage.setItem('isSave', JSON.stringify(true));
}

function setSaveBtnEnabled() {
    $('#save').removeClass('disabled');
    localStorage.setItem('isSave', JSON.stringify(false));
}

function trashButtonClick(trash, removeElem, place, places, chosenBalloons) {
    trash.click(function () {
        let index = chosenBalloons.indexOf(place.id);

        chosenBalloons.splice(index, 1);
        localStorage.setItem('chosenBalloons', JSON.stringify(chosenBalloons));

        places = filterPlaces(places, chosenBalloons);
        //places.splice(index, 1);
        localStorage.setItem('directionPlaces', JSON.stringify(places));


        drawDirectionOnMap(places);


        console.log(chosenBalloons);
        sendPostBalloons(chosenBalloons);
        removeElem.remove();

        setSaveBtnEnabled();
    });
}

function drawDirectionOnMap(places) {
    removeAllPlacesOnMap();

    for (let i = 0; i < places.length; i++) {
        directionCoords.push([places[i].markerInfo.x, places[i].markerInfo.y]);
    }
    var multiRoute = new ymaps.multiRouter.MultiRoute({
        referencePoints: directionCoords,
    }, {
        boundsAutoApply: true
    });
    myMap.geoObjects.add(multiRoute);
}

function upArrowClick(upArrow, place, places, chosenBalloons) {
    upArrow.click(function () {
        chosenBalloons = swapBack(chosenBalloons, chosenBalloons.indexOf(place.id));

        changePage(chosenBalloons, places);

        setSaveBtnEnabled();
    });
}

function downArrowClick(downArrow, place, places, chosenBalloons) {
    downArrow.click(function () {
        chosenBalloons = swapForward(chosenBalloons, chosenBalloons.indexOf(place.id));

        changePage(chosenBalloons, places);

        setSaveBtnEnabled();
    });
}

function changePage(chosenBalloons, places) {
    $('.short-places').empty();
    localStorage.setItem('chosenBalloons', JSON.stringify(chosenBalloons));
    places = filterPlaces(places, chosenBalloons);
    localStorage.setItem('directionPlaces', JSON.stringify(places));
    drawDirectionOnMap(places);
    drawShortPlaces(places);
    sendPostBalloons(chosenBalloons);
}

function swapBack(arr, index) {
    let el = arr[index - 1];
    arr[index - 1] = arr[index];
    arr[index] = el;
    return arr;
}

function swapForward(arr, index) {
    let el = arr[index + 1];
    arr[index + 1] = arr[index];
    arr[index] = el;
    return arr;
}

function drawShortPlaces(places) {
    places.forEach(place => {
        let placeElem = $('.example .short-place').clone();

        let img = placeElem.find('.place-img');
        img.attr('src', place.photo[0]);

        let a = placeElem.find('a');
        a.text(place.name);
        a.attr('href', a.attr('href') + place.id);

        let description = placeElem.find('.description');
        description.text(place.description);

        let addresAndTime = placeElem.find('.address-time');
        addresAndTime.text('Адрес: ' + place.address + (place.workTime ? ', Время работы: ' + place.workTime : ""));

        let trash = placeElem.find('.delete-button');
        trashButtonClick(trash, placeElem, place, places, chosenBalloons);

        let upPlace = placeElem.find('.up');
        upArrowClick(upPlace, place, places, chosenBalloons);

        let downPlace = placeElem.find('.down');
        downArrowClick(downPlace, place, places, chosenBalloons);

        $('body .short-places').append(placeElem);
    });
}

function goToDirection() {
    location.href = "/getdirections";
}

function clickSaveButton(saveButton, ballons) {
    saveButton.on('click', function () {
        saveDirection(ballons);
    });
}

function saveDirection(balloons) {
    $.ajax({
        type: 'POST',
        url: '/savedirection',
        data: JSON.stringify(balloons),
        success: function () { $.jGrowl('Маршрут сохранен, можно посмотреть в личном кабинете'); },
        contentType: 'application/json',
    });
}

function sendPostBalloons(balloons) {
    $.ajax({
        type: 'POST',
        url: '/ajaxdirection',
        //method: 'POST',
        data: JSON.stringify(balloons),
        success: function (response, statusText, status) {
            console.log('Запрос успешно отправился, получаем ответ', response);
        },
        error: function (XHR) {
            console.log('Ошибка запроса', XHR);
        },
        //dataType: 'json',
        contentType: 'application/json'
    });
}

function postPlaces(data = [], cb = [], dp = []) {
    $.ajax({
        type: 'POST',
        url: '/ajaxdirection',
        //method: 'POST',
        data: JSON.stringify(data),
        success: function (response, statusText, status) {
            console.log('Запрос успешно отправился, получаем ответ', response);
            localStorage.setItem('chosenBalloons', JSON.stringify(cb));
            localStorage.setItem('directionPlaces', JSON.stringify(dp));
            $('.short-places').empty();
            //location.href = "/getdirections";
            removeAllPlacesOnMap();
            return true;
        },
        error: function (XHR) {
            console.log('Ошибка запроса', XHR);
        },
        //dataType: 'json',
        contentType: 'application/json'
    });
}

function init(places) {
    // Создание карты.
    return function () {
        myMap = new ymaps.Map("map", {
            // Координаты центра карты.
            // Порядок по умолчанию: «широта, долгота».
            // Чтобы не определять координаты центра карты вручную,
            // воспользуйтесь инструментом Определение координат.
            center: [53.54, 27.33],
            // Уровень масштабирования. Допустимые значения:
            // от 0 (весь мир) до 19.d
            zoom: 7
        }), directionCoords = [];

        //let placeMark = new ymaps.Placemark(myMap.getCenter(),{baloonContent = 'Минск город герой'});

        for (let i = 0; i < places.length; i++) {
            directionCoords.push([places[i].markerInfo.x, places[i].markerInfo.y]);


            /*var myPlacemark = new ymaps.Placemark([54.897225, 29.568142], {
                // Чтобы балун и хинт открывались на метке, необходимо задать ей определенные свойства.
                balloonContentHeader: "Термин метки",
                balloonContentBody: "Описание <em>метки</em>",
                balloonContentFooter: "Подпись метки",
                hintContent: "Надпись при наведении",
                myId: 'first',
            });*/

        }
        //myMap.geoObjects.add(myPlacemarkCollection);


        /////////////////////////////////////////////////////////////////
        //текущее местоположение
        /*var location = ymaps.geolocation.get();
        location.then(function(result) {
            console.log(result.geoObjects.position[0]);

            directionCoords.push([result.geoObjects.position[0], result.geoObjects.position[1]]);
        });*/

        var multiRoute = new ymaps.multiRouter.MultiRoute({
            // Точки маршрута. Точки могут быть заданы как координатами, так и адресом.
            referencePoints: directionCoords,
        }, {
            // Автоматически устанавливать границы карты так,
            // чтобы маршрут был виден целиком.
            boundsAutoApply: true
        });

        myMap.geoObjects.add(multiRoute);
    }
};

function filterPlaces(places, ballons) {
    let newPlaces = [];
    for (let i = 0; i < ballons.length; i++) {
        let place = getPlaceByBallon(places, ballons[i]);
        newPlaces.push(place);
    }
    return newPlaces;
}

function getPlaceByBallon(places, balloon) {
    return places.filter(elem => elem.id === balloon)[0];
}

function removeAllPlacesOnMap() {
    if (myMap) {
        myMap.geoObjects.removeAll();
        directionCoords = [];
    }
}