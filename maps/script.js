// Функция ymaps.ready() будет вызвана, когда
    // загрузятся все компоненты API, а также когда будет готово DOM-дерево.
    ymaps.ready(init);
    function init(){
        // Создание карты.
        var myMap = new ymaps.Map("map", {
            // Координаты центра карты.
            // Порядок по умолчанию: «широта, долгота».
            // Чтобы не определять координаты центра карты вручную,
            // воспользуйтесь инструментом Определение координат.
            center: [53.54, 27.33],
            // Уровень масштабирования. Допустимые значения:
            // от 0 (весь мир) до 19.
            zoom: 7
        });
        //let placeMark = new ymaps.Placemark(myMap.getCenter(),{baloonContent = 'Минск город герой'});
        var myPlacemark = new ymaps.Placemark([51.897225, 27.568142], {
            // Чтобы балун и хинт открывались на метке, необходимо задать ей определенные свойства.
            balloonContentHeader: "Термин метки",
            balloonContentBody: "Описание <em>метки</em>",
            balloonContentFooter: "Подпись метки",
            hintContent: "Надпись при наведении",
            myId: 'first',
        });

        let myPlaceMarkCollection = new ymaps.GeoObjectCollection();

        myPlaceMarkCollection.add(myPlacemark);

        myMap.geoObjects.add(myPlaceMarkCollection);

        saveStation(myMap, myPlaceMarkCollection);
    }

    function saveStation(myMap, myPlaceMarkCollection){
        myMap.events.add(['boundschanhe', 'typechange', 'balloonclose'], setLocationHash);
        myPlaceMarkCollection.events.add(['balloonopen'], function(e) {
            lastOpenedBalloon = e.get('target').properties.get('myId');
            setLocationHash();
        });

        setMapStateByHash();

        function getParam (name, location) {
            location = location || window.location.hash;
            let res = location.match(new RegExp('[#&]' + name + '=([^&]*)', 'i'));
            return (res && res[1] ? res[1] : false);
        }

        function setLocationHash () {
            let params = [
                'type=' + myMap.getType().split('#')[1],
                'center=' + myMap.getCenter(),
                'zoom=' + myMap.getZoom(),
            ];
            if(myMap.balloon.isOpen()) {
                params.push('open=' + lastOpenedBalloon);
            }
            window.location.hash = params.join('&');
        }
        function setMapStateByHash() {
            let hashType = getParam('type');
            let hashCenter = getParam('center');
            let hashZoom = getParam('zoom');
            let open = getParam('open');
            if(hashType) {
                myMap.setType('yandex#' + hashType);
            }
            if(hashCenter) {
                myMap.setCenter(hashCenter.split(','));
            }
            if(hashZoom) {
                myMap.setZoom(hashZoom);
            }
            if(open) {
                myPlaceMarkCollection.each(function (geoObj) {
                    let id = geoObj.properties.get('myId');
                    if(id == open) {
                        geoObj.balloon.open();
                    }
                });
            }
        }
    }